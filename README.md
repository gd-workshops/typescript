# Воркшоп "Погружение в TypeScript"

## Цель:

- научиться манипулировать типами (соединять, пересекать, разделять);
- научиться использовать перегрузку функций в TS;
- научиться создавать дженерик-функции;
- научиться строить взаимодействия между несколькими дженерик-типами.

## Порядок выполнения заданий:

1. [typesIntercations](./tasks/typesIntercations.ts) -- манипуляции с типами, здесь мы научимся строить новые типы из уже существующих;
2. [utilityTypes](./tasks/utilityTypes.ts) -- в этом задании мы научимся использовать utility-типы для решения типичных задач на проекте;
3. [functionOverloading](./tasks/functionOverloading.ts) -- в этом задании мы научимся делать перегрузку функций в TypeScript;
4. [deepPartial](./tasks/deepPartial.ts) -- в этом задании мы попробуем усовершенствовать utility-тип Partial так, чтобы он делал опциональными поля вложенных типов;
5. [genericGetValue](./tasks/genericGetValue.ts) -- в этом задании мы сделаем generic-функцию с несколькими generic-типами и их взаимодействиями.
